<?php
   header('status: 503 Expired Website');
?>

<!doctype html>
<html lang="en">
   <head>
      <base href="https://csclub.uwaterloo.ca/~sysadmin/expired/" />
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Computer Science Club of the University of Waterloo</title>

      <link href="./css/disabled.css" rel="stylesheet" />
   </head>
   <body>
      <div class="wrapper">
         <header>
            <div class="logos">
               <span class="logo">
                  <a href="https://uwaterloo.ca/"><img  src="./img/uwaterloo.png"
                        alt="University of Waterloo"
                        class="uwaterloo" /></a>
               </span>
               <span class="logo">
                  <a href="https://csclub.uwaterloo.ca/"><img  src="./img/csc.png"
                        alt="Computer Science Club of the University of Waterloo"
                        class="csc" /></a>
               </span>
            </div>
         </header>
         <section class="body">
            <h1>Parked Website</h1>
            <p>This is a parking page for the website of a previous <a href="https://csclub.uwaterloo.ca/">Computer Science Club</a> member.</p>

            <p>If you are the owner and would like to continue using our web hosting, please renew your membership to restore access to your website.</p>

            <p>We apologize for any inconvenience this may cause. If you have any questions, please contact <a href="mailto:syscom@csclub.uwaterloo.ca">syscom@csclub.uwaterloo.ca</a>.</p>
         </section>
      </div>
   </body>
</html>
